import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Container, Header, Left, Title, Text, Body, Icon } from "native-base";


export default class Test extends Component {
  static navigationOptions = {
    header:null
  }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Icon style = {style.leftIcon} name = "menu" onPress = {() => this.props.navigation.openDrawer()}></Icon>
          </Left>
          
          <Body>
            <Title>
              <Text style = {style.headerTitle}>WittyPlay</Text>
            </Title>
          </Body>
        </Header>
        <Text>Test Section</Text>
      </Container>
    )
  }
}

const style = StyleSheet.create({
  leftIcon: {
    color:'#ffffff',
  },
  headerTitle:{
    color:'#ffffff'
  }
})