import React, { Component } from "react";
import Topic from "../topics/topics";
import { StyleSheet } from "react-native";
import { Container, Header, Left, Title, Text, Body, Icon, Content } from "native-base";

export default class Learn extends Component {
  static navigationOptions = {
    header:null,
    drawerIcon:({tintColor}) => (
      <Icon name ="home"  style = {{fontSize:24, color:tintColor}}/>
    )
  };
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Icon style = {style.leftIcon} name = "menu" onPress = {() => this.props.navigation.openDrawer()}></Icon>
          </Left>
          
          <Body>
            <Title>
              <Text style = {style.headerTitle}>WittyPlay</Text>
            </Title>
          </Body>
        </Header>
        <Content>
          <Topic navigation = { this.props.navigation }></Topic>
        </Content>
        
      </Container>
    )
  }
}

const style = StyleSheet.create({
  leftIcon: {
    color:'#ffffff',
  },
  headerTitle:{
    color:'#ffffff'
  }
})