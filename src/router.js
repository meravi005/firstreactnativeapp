import React from "react";
import { 
  createAppContainer, 
  createDrawerNavigator, 
  createStackNavigator,
  DrawerItems } from "react-navigation";

import { SafeAreaView, ScrollView, View, Text} from "react-native"
import Learn from "./screens/learn/learn";
import Test from "./screens/test/test";
import TestPreview from "./screens/test-preview/test-preview";

import TopicPreview from "./screens/topic-preview/topic-preview";
import Topics from "./screens/topics/topics";

const LearnSection = createStackNavigator({
  Learn:Learn,
  TopicPreview:TopicPreview,
  Topics:Topics
}, {
  initialRouteName:'Learn'
});

const TestSection = createStackNavigator({
  Test:Test,
  TestPreview:TestPreview
})

const customDrawerComponent = props => (
  <SafeAreaView>
    <View style = {{height:60, backgroundColor:'#2776bb'}}>
      <Text>WittyPlay</Text>
    </View>
    <ScrollView>
      <DrawerItems {...props }></DrawerItems>
    </ScrollView>
  </SafeAreaView>
)

const DrawerNavigation = createDrawerNavigator({
  Learn:LearnSection,
  Test:TestSection
}, {
  initialRouteName:'Learn',
  contentComponent:customDrawerComponent,
  contentOptions: {
    activeTintColor:'orange'
  }
});




export default AppContainer = createAppContainer(DrawerNavigation)